# Git Repository for the ML class.

## Getting Started
1. Download and install Python 3 [Anaconda](https://www.anaconda.com/download/).
2. Download the contents of this repository either by git (```git clone https://gitlab.com/victorkwak/MLClass``` from you terminal) or by using download button on this page.
2. If you haven't already, open up your terminal, navigate to the directory where you downloaded this repo and type in "jupyter notebook". For more detailed instructions, [look here](http://jupyter.readthedocs.io/en/latest/running.html).
3. You should see a web application open that looks like this: ![](images/notebook.png)
4. Navigate to and open up whichever notebook you want. 

